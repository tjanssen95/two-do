import { AngularTwoDoPage } from './app.po';

describe('angular-two-do App', () => {
  let page: AngularTwoDoPage;

  beforeEach(() => {
    page = new AngularTwoDoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
